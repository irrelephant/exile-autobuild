var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');
var LocalStrategy = require('passport-local').Strategy;

var cfg = require('./cfg');

var routes = require('./routes/index');
var api = require('./routes/api');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({ secret: 'Shiroi Mokona' }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
    
app.use('/', routes);
app.use('/users', users);
app.user('/api', api);

passport.use(new LocalStrategy(function(username, password, done) {
    return done(null, { username: username });
}));

passport.serializeUser(function(user, done) {
    done(null, user.username);
});

passport.deserializeUser(function(username, done) {
    done(null, { username: username });
});

(function registerNotFoundHandler() {
    app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
    });
})();

(function registerDevelopmentErrHandler() {
    if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
	    res.status(err.status || 500);
	    res.render('error', {
		message: err.message,
		error: err,
		serverName: cfg.serverName,
		title: 'Status 500'
	    });
	});
    }
})();

(function registerErrHandler() {
    app.use(function(err, req, res, next) {
	res.status(err.status || 500);
	res.render('error', {
	    message: err.message,
	    error: {},
	    serverName: cfg.serverName,
	    title: 'Status 500'
	});
    });
})();

module.exports = app;
