var express = require('express');
var router = express.Router();
var passport = require('passport');

var cfg = require('../cfg');

function localAuth() {
    return passport.authenticate('local', { successRedirect: '/', failureFlash: true, failureRedirect: '/login' });
}

router.post('/login', passport.authenticate('local',
					    { successRedirect: '/',
					      failureRedirect: '/login',
					      failureFlash: true }));

router.get('/login', function(req, res, done) {
    res.render('login', { serverName: cfg.serverName });
});

router.get('/logout', function(req, res, done) {
    req.logout();
    res.redirect('/login');
});

router.get('/*', function(req, res, next) {
    if (!req.user) {
	res.redirect('/login');
    } else next();
});

router.get('/', function(req, res, next) {
    res.render('index', { title: 'Home', serverName: cfg.serverName, user: req.user });
});

module.exports = router;
