var switchingTab = false;

function switchTab(btnClass, tabClass, tabName) {
    return function() {
	if (switchingTab) return;

	if ($('.' + btnClass + '.active-tab').length) return;

	switchingTab = true;
	$('.active-tab').removeClass('active-tab');
	
	$('.tab').fadeOut(100, function() {
	    switchingTab = false;
	    setTimeout(function() { $('.tab.' + tabClass).fadeIn(100); }, 100);
	    $('.' + btnClass).addClass('active-tab');
	});
	
	$('h1.page-name').fadeOut(100, function() {
	    $(this).text(tabName).fadeIn(100);
	});
    }
}

$(function() {
    $('.logout').click(function() {
        window.location.href = '/logout';
    });

    $('.jobs').click(switchTab('jobs', 'jobs-tab', 'Scheduled Jobs'));
    $('.commands').click(switchTab('commands', 'commands-tab', 'Commands'));
    $('.info').click(switchTab('info', 'info-tab', 'Information'));
});
